// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseAPIKey: "AIzaSyBvOw17g_fq7P7hQFrJf9QG67R1waHn3tQ",
  firebaseUrl: "https://lam-2021-a-f5c56-default-rtdb.firebaseio.com/",
  mapsApiKey: "AIzaSyDQT9_ljvvQ8OvwJ3QxGgWghfd5lTkhfnU"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
