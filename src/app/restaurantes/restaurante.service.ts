import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Restaurante } from './restaurante.model';
import { map, tap } from 'rxjs/operators';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestauranteService {

  private _restaurantes = new BehaviorSubject<Restaurante[]>([])

  get restaurnates() {
    return this._restaurantes.asObservable();
  }

  constructor(private http: HttpClient) { }
  addRestaurante(restaurante: Restaurante) {
    this.http.post<any>(environment.firebaseUrl + 'restaurantes.json', { ...restaurante }).subscribe(data => {
      console.log(data);
    });
  }

  fetchRestaurantes() {
    return this.http.get<{ [key: string]: Restaurante }>(
      environment.firebaseUrl + 'restaurantes.json'
    )
      .pipe(map(dta => {
        const rests = [];
        for (const key in dta) {
          if (dta.hasOwnProperty(key)) {
            rests.push(
              new Restaurante(key, dta[key].titulo, dta[key].imgUrl, dta[key].platillos, dta[key].lat, dta[key].lng
              ));
          }
        }
        return rests;
      }),
        tap(rest => {
          this._restaurantes.next(rest);
        }));
  }


  getRestaurante(restauranteId: string) {
    const url = environment.firebaseUrl + `restaurantes/${restauranteId}.json`;
    return this.http.get<Restaurante>(url)
      .pipe(map(dta => {
        return new Restaurante(restauranteId, dta.titulo, dta.imgUrl, dta.platillos, dta.lat, dta.lng);
      }));
  }





}
