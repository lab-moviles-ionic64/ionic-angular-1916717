import { map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Ofertas } from './ofertas.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OfertasService {
  private _ofertas = new BehaviorSubject<Ofertas[]>([])
  get ofertas() {
    return this._ofertas.asObservable()
  }
  constructor(
    private http: HttpClient
  ) { }

  addOferta(oferta: Ofertas) {
    this.http.post<any>(environment.firebaseUrl + "ofertas.json", { ...oferta }).subscribe(data => {
      console.log(data)
    })
  }

  fetchOfertas() {
    return this.http.get<Ofertas[]>(environment.firebaseUrl + "ofertas.json").pipe(map(dta => {
      const ofertas = []
      for (const key in dta) {
        if (dta.hasOwnProperty(key)) {
          ofertas.push(new Ofertas(
            key,
            dta[key].restaurante,
            dta[key].imagen,
            dta[key].titulo,
            dta[key].subtitulo,
            dta[key].vigencia,
          ));
        }
      }
      return ofertas;
    }), tap(ofertas => {
      this._ofertas.next(ofertas)
    }
    ))
  }


}
