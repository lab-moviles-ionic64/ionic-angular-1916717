import { Ofertas } from './ofertas.model';
import { Component, OnInit } from '@angular/core';
import { OfertasService } from './ofertas.service';
import { ViewWillEnter } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-ofertas',
  templateUrl: './ofertas.page.html',
  styleUrls: ['./ofertas.page.scss'],
})
export class OfertasPage implements OnInit, ViewWillEnter {

  ofertas: Ofertas[] = [];
  ofertasSub: Subscription
  isLoading = true

  constructor(private ofertasService: OfertasService) { }
  ionViewWillEnter(): void {
    this.isLoading = true;
    this.ofertasSub = this.ofertasService.fetchOfertas().subscribe(data => {
      this.isLoading = false;
    })
  }

  ngOnInit() {
    this.ofertasSub = this.ofertasService.ofertas.subscribe(data => {
      this.ofertas = data;
    })
  }

  ngOnDestroy() {
    console.log('ANGULAR -> ngOnDestroy');
    if (this.ofertasSub)
      this.ofertasSub.unsubscribe()
  }

}
