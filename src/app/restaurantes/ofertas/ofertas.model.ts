export class Ofertas {
    constructor(
        public id: string,
        public restaurante: string,
        public imagen: string,
        public titulo: string,
        public subtitulo: string,
        public vigencia: string,
    ) { }
}