import { RestauranteService } from './restaurante.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Restaurante } from './restaurante.model';

@Component({
  selector: 'app-restaurantes',
  templateUrl: './restaurantes.page.html',
  styleUrls: ['./restaurantes.page.scss'],
})
export class RestaurantesPage implements OnInit {
  restaurantes: Restaurante[] = [];
  constructor() { }
  ngOnInit() {
  }
}