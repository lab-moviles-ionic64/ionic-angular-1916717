import { ReservacionesPageModule } from './../reservaciones/reservaciones.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RestaurantesPageRoutingModule } from './restaurantes-routing.module';

import { RestaurantesPage } from './restaurantes.page';
import { RestauranteDetallePage } from './restaurante-detalle/restaurante-detalle.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RestaurantesPageRoutingModule,
    ReservacionesPageModule
  ],
  declarations: [RestaurantesPage]
})
export class RestaurantesPageModule { }
