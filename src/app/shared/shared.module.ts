import { MapModalComponent } from './map-modal/map-modal.component';
import { LocationPickerComponent } from './pickers/location-picker/location-picker.component';
import { IonicModule } from '@ionic/angular';
import { ImagePickerComponent } from './pickers/image-picker/image-picker.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [
    ImagePickerComponent,
    LocationPickerComponent,
    MapModalComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    ImagePickerComponent,
    LocationPickerComponent,
    MapModalComponent
  ]
})
export class SharedModule { }
