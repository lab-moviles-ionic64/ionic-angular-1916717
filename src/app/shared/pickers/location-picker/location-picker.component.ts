import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { MapModalComponent } from '../../map-modal/map-modal.component';

@Component({
  selector: 'app-location-picker',
  templateUrl: './location-picker.component.html',
  styleUrls: ['./location-picker.component.scss'],
})
export class LocationPickerComponent implements OnInit {

  selectedLocationImage: string;

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() { }

  onPickLocation() {
    this.modalCtrl.create({ component: MapModalComponent }).then(modalEl => {
      modalEl.onDidDismiss().then(modalData => {
        console.log(modalData);
        this.selectedLocationImage = this.selectedLocationImage = `https://maps.googleapis.com/maps/api/staticmap?
        center=${modalData.data.lat},${modalData.data.lng}&zoom=16&size=600x400&mapty
        pe=roadmap&markers=color:red%7Clabel:Lugar%7C${modalData.data.lat},
        ${modalData.data.lng}&key=${environment.mapsApiKey}`
      });
      modalEl.present();
    });
  }
}
