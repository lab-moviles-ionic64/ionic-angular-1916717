import { NgForm } from '@angular/forms';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { IonDatetime, ModalController } from '@ionic/angular';
import { Restaurante } from 'src/app/restaurantes/restaurante.model';

@Component({
  selector: 'app-nueva-reservacion',
  templateUrl: './nueva-reservacion.component.html',
  styleUrls: ['./nueva-reservacion.component.scss'],
})
export class NuevaReservacionComponent implements OnInit {
  @Input() restaurante: Restaurante;
  @Input() mode: 'select' | 'hoy';

  @ViewChild('formNew') myForm: NgForm;

  fecha: IonDatetime
  nombre:string
  constructor(
    private modalCtrl: ModalController,
  ) { }
  ngOnInit() {
  }
  onReservar() {
    console.log(this.myForm.value['horario'])

    this.modalCtrl.dismiss({
      restaurante: this.restaurante,
      nombre: this.myForm.value['nombre'],
      horario: new Date(this.myForm.value['horario']).toLocaleDateString("es-Es", {
        day: "numeric",
        month: "long",
        year: "numeric",
        weekday: "long"
      })
    }, 'confirm');
  }
  onCancel() {
    this.modalCtrl.dismiss(null,
      'cancel');
  }

}
