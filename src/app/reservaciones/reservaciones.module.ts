import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReservacionesPageRoutingModule } from './reservaciones-routing.module';

import { ReservacionesPage } from './reservaciones.page';
import { NuevaReservacionComponent } from './nueva-reservacion/nueva-reservacion.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReservacionesPageRoutingModule
  ],
  declarations: [
    ReservacionesPage,
    NuevaReservacionComponent
  ],
  exports: [
    NuevaReservacionComponent
  ]
})
export class ReservacionesPageModule { }
