export class Reservaciones {
    constructor(
        public id: string,
        public imagen: string,
        public restaurante: string,
        public fecha: string,
        public restauranteId: string,
        public usuarioId: string,
        public nombre: string,
    ){}
}