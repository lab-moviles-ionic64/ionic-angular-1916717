import { LoginService } from './../login/login.service';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { Restaurante } from './../restaurantes/restaurante.model';
import { Reservaciones } from './reservaciones.model';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map, switchMap, take, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReservacionesService {

  private _reservaciones = new BehaviorSubject<Reservaciones[]>([]);

  get reservaciones() {
    return this._reservaciones.asObservable();
  }

  usuarioId = null;

  constructor(
    private http: HttpClient,
    private loginService: LoginService
  ) {
    this.loginService.usuarioId.subscribe(usuarioId => {
      this.usuarioId = usuarioId
    })
  }

  fetchReservaciones() {
    return this.http.get<{ [key: string]: Reservaciones }>(
      environment.firebaseUrl + 'reservaciones.json?orderBy="usuarioId"&equalTo="' + this.usuarioId + '"'
    ).pipe(map(dta => {
      const rests = [];
      for (const key in dta) {
        if (dta.hasOwnProperty(key)) {
          rests.push(new Reservaciones(
            key,
            dta[key].imagen,
            dta[key].restaurante,
            dta[key].fecha,
            dta[key].restauranteId,
            dta[key].usuarioId,
            dta[key].nombre
          ));
        }
      }
      return rests;
    }), tap(rest => {
      this._reservaciones.next(rest);
    }));
  }

  removeReservacion(reservacionId: string) {
    let url = `${environment.firebaseUrl}reservaciones/${reservacionId}.json`;
    return this.http.delete(url)
      .pipe(switchMap(() => {
        return this.reservaciones;
      }), take(1), tap(rsvs => {
        this._reservaciones.next(rsvs.filter(r => r.id !== reservacionId))
      }))
  }

  getReservacion(reservacionId: string) {
    const url = environment.firebaseUrl + `reservaciones/${reservacionId}.json`;

    return this.http.get<Reservaciones>(url)
      .pipe(map(dta => {
        return new Reservaciones(
          reservacionId,
          dta.imagen,
          dta.restaurante,
          dta.fecha,
          dta.restauranteId,
          dta.usuarioId,
          dta.nombre,
          );
      }));
  }

  updateReservacion(reservacionId: string, reservacion: Reservaciones) {
    const url = environment.firebaseUrl + `reservaciones/${reservacionId}.json`;
    this.http.put<any>(url, { ...reservacion }).subscribe(data => {
      console.log(data);
    });
  }

  addReservacion(restaurante: Restaurante, nombre: string, horario: string) {
    const rsv = new Reservaciones(
      null,
      restaurante.imgUrl,
      restaurante.titulo,
      horario,
      restaurante.id,
      this.usuarioId,
      nombre
    );
    this.http.post<any>(environment.firebaseUrl + 'reservaciones.json', { ...rsv }).subscribe(data => {
      console.log(data);
    });

  }


}


